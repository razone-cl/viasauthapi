﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ViasChileAuthAPI.Modelos;

namespace ViasChileAuthAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonaSistemasController : ControllerBase
    {
        private readonly SISACCESOContext _context;

        public PersonaSistemasController(SISACCESOContext context)
        {
            _context = context;
        }

        // GET: api/PersonaSistemas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PersonaSistemas>>> GetPersonaSistemas()
        {
            return await _context.PersonaSistemas.ToListAsync();
        }

        // GET: api/PersonaSistemas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PersonaSistemas>> GetPersonaSistemas(int id)
        {
            var personaSistemas = await _context.PersonaSistemas.FindAsync(id);

            if (personaSistemas == null)
            {
                return NotFound();
            }

            return personaSistemas;
        }

        // PUT: api/PersonaSistemas/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPersonaSistemas(int id, PersonaSistemas personaSistemas)
        {
            if (id != personaSistemas.PeisCod)
            {
                return BadRequest();
            }

            _context.Entry(personaSistemas).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PersonaSistemasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PersonaSistemas
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<PersonaSistemas>> PostPersonaSistemas(PersonaSistemas personaSistemas)
        {
            _context.PersonaSistemas.Add(personaSistemas);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPersonaSistemas", new { id = personaSistemas.PeisCod }, personaSistemas);
        }

        // DELETE: api/PersonaSistemas/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<PersonaSistemas>> DeletePersonaSistemas(int id)
        {
            var personaSistemas = await _context.PersonaSistemas.FindAsync(id);
            if (personaSistemas == null)
            {
                return NotFound();
            }

            _context.PersonaSistemas.Remove(personaSistemas);
            await _context.SaveChangesAsync();

            return personaSistemas;
        }

        private bool PersonaSistemasExists(int id)
        {
            return _context.PersonaSistemas.Any(e => e.PeisCod == id);
        }
    }
}
