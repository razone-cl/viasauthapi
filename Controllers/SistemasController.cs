﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ViasChileAuthAPI.Modelos;

namespace ViasChileAuthAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SistemasController : ControllerBase
    {
        private readonly SISACCESOContext _context;

        public SistemasController(SISACCESOContext context)
        {
            _context = context;
        }

        // GET: api/Sistemas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Sistemas>>> GetSistemas()
        {
            return await _context.Sistemas.ToListAsync();
        }

        // GET: api/SistemasDetalle/5
        [HttpGet("GetSistemasDetalles/{id}")]
        public async Task<ActionResult<Sistemas>> GetSistemasDetalles(int id)
        {
            //Eager Loading
            //var sistemas = await _context.Sistemas
            //    .Include(sistemas => sistemas.SistemaPerfiles)
            //        .ThenInclude(personasistemas => personasistemas.PersonaSistemas)
            //    .Where(sistemas => sistemas.SistCod == id).FirstOrDefaultAsync();


            //Explicit Loading
            var sistemas = await _context.Sistemas.SingleAsync(sistemas => sistemas.SistCod == id);

            _context.Entry(sistemas)
                .Collection(sist => sist.SistemaPerfiles)
                //Where
                .Load();


            //_context.Entry(sistemas)
            //  .Collection(sist => sist.PersonaSistemas)
            //  .Where(sistemas.Perfiles ==12)
            //  .Load();


            //public void ChangePassword(int userId, string password)
            //{
            //    var user = new User() { Id = userId, Password = password };
            //    using (var db = new MyEfContextName())
            //    {
            //        db.Users.Attach(user);
            //        db.Entry(user).Property(x => x.Password).IsModified = true;
            //      //ValidateOnSaveEnabled = false;
            //      //if (db.Entry(user).Property(x => x.Password).GetValidationErrors().Count == 0)
            //        db.SaveChanges();
            //    }
            //}


            if (sistemas == null)
            {
                return NotFound();
            }

            return sistemas;
        }
        [HttpGet("PostSistemasDetalles/")]
        public async Task<ActionResult<Sistemas>> PostSistemasDetalles()
        {
            var sistema = new Sistemas();
            sistema.SistNombre = "Sistema De Prueba Insertado por POST 4 con hijos";
            sistema.VigeCod = 1;
            sistema.IdUsuarioEncargado = 3587;
            sistema.SistFechaDesde = DateTime.UtcNow;
            sistema.SistFechaHasta = DateTime.Now.AddDays(365);

            SistemaPerfiles perfil1 = new SistemaPerfiles();//Perfil Usuario
            perfil1.PerfCod=12;
            perfil1.VigeCod = 1;
            perfil1.SipeUltUpdate = DateTime.Now;

            SistemaPerfiles perfil2 = new SistemaPerfiles();//Perfil Admin General
            perfil2.PerfCod = 1;
            perfil2.SipeUltUpdate = DateTime.Now;
            perfil2.VigeCod = 2;

            sistema.SistemaPerfiles.Add(perfil1);
            sistema.SistemaPerfiles.Add(perfil2);

            //var sistemaperfil = _context.SistemaPerfiles.
            //    Where(perfil1 => perfil1.SipeCod == 12)
            //    .SingleOrDefault();           

            _context.Sistemas.Add(sistema);
            _context.SaveChanges();
            var sistemas = await _context.Sistemas
                .Include(sistemas => sistemas.SistemaPerfiles)
                    .ThenInclude(personasistemas => personasistemas.PersonaSistemas)
                .Where(sistemas => sistemas.SistCod == sistema.SistCod).FirstOrDefaultAsync();

            if (sistemas == null)
            {
                return NotFound();
            }

            return sistemas;
        }

        // GET: api/Sistemas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Sistemas>> GetSistemas(int id)
        {
            var sistemas = await _context.Sistemas.FindAsync(id);

            if (sistemas == null)
            {
                return NotFound();
            }

            return sistemas;
        }

        // PUT: api/Sistemas/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSistemas(int id, Sistemas sistemas)
        {
            if (id != sistemas.SistCod)
            {
                return BadRequest();
            }

            _context.Entry(sistemas).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SistemasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Sistemas
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Sistemas>> PostSistemas(Sistemas sistemas)
        {
            _context.Sistemas.Add(sistemas);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSistemas", new { id = sistemas.SistCod }, sistemas);
        }

        // DELETE: api/Sistemas/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Sistemas>> DeleteSistemas(int id)
        {
            var sistemas = await _context.Sistemas.FindAsync(id);
            if (sistemas == null)
            {
                return NotFound();
            }

            _context.Sistemas.Remove(sistemas);
            await _context.SaveChangesAsync();

            return sistemas;
        }

        private bool SistemasExists(int id)
        {
            return _context.Sistemas.Any(e => e.SistCod == id);
        }
    }
}
