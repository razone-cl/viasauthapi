﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Perfiles
    {
        public Perfiles()
        {
            Administradores = new HashSet<Administradores>();
            PerfilModulos = new HashSet<PerfilModulos>();
            Usuarios = new HashSet<Usuarios>();
        }

        public int PerfCod { get; set; }
        public string PerfNombre { get; set; }
        public string PerfDescripcion { get; set; }
        public int VigeCod { get; set; }

        public virtual Vigencias VigeCodNavigation { get; set; }
        public virtual ICollection<Administradores> Administradores { get; set; }
        public virtual ICollection<PerfilModulos> PerfilModulos { get; set; }
        public virtual ICollection<Usuarios> Usuarios { get; set; }
    }
}
