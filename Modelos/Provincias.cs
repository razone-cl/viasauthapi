﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Provincias
    {
        public Provincias()
        {
            Comunas = new HashSet<Comunas>();
            EmpresaExternas = new HashSet<EmpresaExternas>();
        }

        public int ProvCod { get; set; }
        public string ProvNombre { get; set; }
        public int RegiCod { get; set; }
        public int VigeCod { get; set; }

        public virtual Regiones RegiCodNavigation { get; set; }
        public virtual ICollection<Comunas> Comunas { get; set; }
        public virtual ICollection<EmpresaExternas> EmpresaExternas { get; set; }
    }
}
