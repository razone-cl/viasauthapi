﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Modulos
    {
        public Modulos()
        {
            PerfilModulos = new HashSet<PerfilModulos>();
        }

        public int ModuCod { get; set; }
        public int? ModuCodPadre { get; set; }
        public string ModuNombre { get; set; }
        public string ModuControlador { get; set; }
        public string ModuVista { get; set; }
        public int VigeCod { get; set; }
        public int? ModuOrden { get; set; }
        public int? MenuCod { get; set; }
        public string ModuAyuda { get; set; }

        public virtual ICollection<PerfilModulos> PerfilModulos { get; set; }
    }
}
