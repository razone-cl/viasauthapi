﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class SistemaPerfiles
    {
        public SistemaPerfiles()
        {
            PersonaSistemas = new HashSet<PersonaSistemas>();
        }

        public int SipeCod { get; set; }
        public int SistCod { get; set; }
        public int PerfCod { get; set; }
        public int VigeCod { get; set; }
        public DateTime? SipeUltUpdate { get; set; }

        public virtual Perfilamientos PerfCodNavigation { get; set; }
        public virtual Sistemas SistCodNavigation { get; set; }
        public virtual Vigencias VigeCodNavigation { get; set; }
        public virtual ICollection<PersonaSistemas> PersonaSistemas { get; set; }
    }
}
