﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Regiones
    {
        public Regiones()
        {
            EmpresaExternas = new HashSet<EmpresaExternas>();
            Provincias = new HashSet<Provincias>();
        }

        public int RegiCod { get; set; }
        public string RegiNombre { get; set; }
        public string RegiNumero { get; set; }
        public int VigeCod { get; set; }

        public virtual ICollection<EmpresaExternas> EmpresaExternas { get; set; }
        public virtual ICollection<Provincias> Provincias { get; set; }
    }
}
