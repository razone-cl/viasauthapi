﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class TipoAtributos
    {
        public TipoAtributos()
        {
            Atributos = new HashSet<Atributos>();
        }

        public int TiatCod { get; set; }
        public string TiatNombre { get; set; }
        public int? VigeCod { get; set; }

        public virtual Vigencias VigeCodNavigation { get; set; }
        public virtual ICollection<Atributos> Atributos { get; set; }
    }
}
