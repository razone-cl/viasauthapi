﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Fotografias
    {
        public int FotoCod { get; set; }
        public int PersCod { get; set; }
        public string FotoRut { get; set; }
        public string FotoDv { get; set; }
        public byte[] FotoImg { get; set; }
    }
}
