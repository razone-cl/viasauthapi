﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Atributos
    {
        public Atributos()
        {
            PersonaAtributos = new HashSet<PersonaAtributos>();
        }

        public int AtriCod { get; set; }
        public string AtriNombre { get; set; }
        public int? GratCod { get; set; }
        public int? TiatCod { get; set; }
        public int? AtriMaximo { get; set; }
        public string AtriApiLista { get; set; }
        public string AtriApiRegistro { get; set; }
        public int? AtriOrden { get; set; }
        public int? VigeCod { get; set; }

        public virtual GrupoAtributos GratCodNavigation { get; set; }
        public virtual TipoAtributos TiatCodNavigation { get; set; }
        public virtual ICollection<PersonaAtributos> PersonaAtributos { get; set; }
    }
}
