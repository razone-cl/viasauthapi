﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Descripciones
    {
        public Descripciones()
        {
            CargoDescripciones = new HashSet<CargoDescripciones>();
        }

        public int DescCod { get; set; }
        public int? VigeCod { get; set; }
        public string DescCodigo { get; set; }
        public string DescDetalle { get; set; }

        public virtual Vigencias VigeCodNavigation { get; set; }
        public virtual ICollection<CargoDescripciones> CargoDescripciones { get; set; }
    }
}
