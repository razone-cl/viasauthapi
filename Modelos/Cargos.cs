﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Cargos
    {
        public Cargos()
        {
            CargoDescripciones = new HashSet<CargoDescripciones>();
            Recursos = new HashSet<Recursos>();
        }

        public int CargCod { get; set; }
        public int? TicaCod { get; set; }
        public int? VigeCod { get; set; }
        public string CargNombre { get; set; }

        public virtual TipoCargos TicaCodNavigation { get; set; }
        public virtual Vigencias VigeCodNavigation { get; set; }
        public virtual ICollection<CargoDescripciones> CargoDescripciones { get; set; }
        public virtual ICollection<Recursos> Recursos { get; set; }
    }
}
