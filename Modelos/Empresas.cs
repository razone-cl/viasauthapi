﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Empresas
    {
        public Empresas()
        {
            EmpresaNiveles = new HashSet<EmpresaNiveles>();
            Usuarios = new HashSet<Usuarios>();
        }

        public int EmprCod { get; set; }
        public string EmprNombre { get; set; }
        public string EmprAbreviatura { get; set; }
        public byte[] EmprLogo { get; set; }
        public string EmprRazonSocial { get; set; }
        public string EmprRut { get; set; }
        public int? VigeCod { get; set; }
        public DateTime? EmprUltUpdate { get; set; }

        public virtual Vigencias VigeCodNavigation { get; set; }
        public virtual ICollection<EmpresaNiveles> EmpresaNiveles { get; set; }
        public virtual ICollection<Usuarios> Usuarios { get; set; }
    }
}
