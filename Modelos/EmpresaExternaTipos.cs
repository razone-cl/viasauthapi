﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class EmpresaExternaTipos
    {
        public EmpresaExternaTipos()
        {
            EmpresaExternas = new HashSet<EmpresaExternas>();
        }

        public int EetiCod { get; set; }
        public string EetiNombre { get; set; }
        public int VigeCod { get; set; }

        public virtual Vigencias VigeCodNavigation { get; set; }
        public virtual ICollection<EmpresaExternas> EmpresaExternas { get; set; }
    }
}
