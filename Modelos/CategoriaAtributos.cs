﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class CategoriaAtributos
    {
        public CategoriaAtributos()
        {
            GrupoAtributos = new HashSet<GrupoAtributos>();
        }

        public int CaatCod { get; set; }
        public string CaatNombre { get; set; }
        public int? CaatOrden { get; set; }
        public int? VigeCod { get; set; }

        public virtual ICollection<GrupoAtributos> GrupoAtributos { get; set; }
    }
}
