﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class PersonaSistemas
    {
        public int PeisCod { get; set; }
        public int PersCod { get; set; }
        public int SipeCod { get; set; }
        public int VigeCod { get; set; }
        public DateTime? PeisUltUpdate { get; set; }

        public virtual Personas PersCodNavigation { get; set; }
        public virtual SistemaPerfiles SipeCodNavigation { get; set; }
        public virtual Vigencias VigeCodNavigation { get; set; }
    }
}
