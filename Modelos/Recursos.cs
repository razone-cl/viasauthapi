﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Recursos
    {
        public Recursos()
        {
            DependenciaRecursosRecuCodNavigation = new HashSet<DependenciaRecursos>();
            DependenciaRecursosRecuCodSubNavigation = new HashSet<DependenciaRecursos>();
        }

        public int RecuCod { get; set; }
        public int EmniCod { get; set; }
        public int CargCod { get; set; }
        public int? PersCod { get; set; }
        public int VigeCod { get; set; }
        public bool? RecuResponsable { get; set; }
        public string RecuTop { get; set; }
        public string RecuLeft { get; set; }
        public string JefeCod { get; set; }
        public string PayrCod { get; set; }
        public string RecuStaff { get; set; }
        public string TipoPers { get; set; }

        public virtual Cargos CargCodNavigation { get; set; }
        public virtual EmpresaNiveles EmniCodNavigation { get; set; }
        public virtual Personas PersCodNavigation { get; set; }
        public virtual Vigencias VigeCodNavigation { get; set; }
        public virtual ICollection<DependenciaRecursos> DependenciaRecursosRecuCodNavigation { get; set; }
        public virtual ICollection<DependenciaRecursos> DependenciaRecursosRecuCodSubNavigation { get; set; }
    }
}
