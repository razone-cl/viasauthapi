﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Personas
    {
        public Personas()
        {
            PersonaAtributos = new HashSet<PersonaAtributos>();
            PersonaSistemas = new HashSet<PersonaSistemas>();
            Recursos = new HashSet<Recursos>();
        }

        public int PersCod { get; set; }
        public int? VigeCod { get; set; }
        public int? TipeCod { get; set; }
        public int? EsciCod { get; set; }
        public string PersNombres { get; set; }
        public string PersApPaterno { get; set; }
        public string PersApMaterno { get; set; }
        public string PersNombreCompleto { get; set; }
        public DateTime? PersFechaNacimiento { get; set; }
        public int? SetiCod { get; set; }
        public string PersRut { get; set; }
        public string PersDv { get; set; }
        public string PersEmail { get; set; }
        public int PemoCod { get; set; }
        public string PersUsername { get; set; }
        public string PersPassword { get; set; }
        public int? CecoCodPers { get; set; }
        public string PersCodAux { get; set; }
        public bool? PersCambioPass { get; set; }
        public bool? PersStaff { get; set; }

        public virtual PersonaModos PemoCodNavigation { get; set; }
        public virtual Vigencias VigeCodNavigation { get; set; }
        public virtual ICollection<PersonaAtributos> PersonaAtributos { get; set; }
        public virtual ICollection<PersonaSistemas> PersonaSistemas { get; set; }
        public virtual ICollection<Recursos> Recursos { get; set; }
    }
}
