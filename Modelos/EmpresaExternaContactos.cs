﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class EmpresaExternaContactos
    {
        public int EmecCod { get; set; }
        public string EmecNombre { get; set; }
        public string EmecTelefono { get; set; }
        public string EmecEmail { get; set; }
        public string EmecCargo { get; set; }
        public DateTime EmecFechaRegistro { get; set; }
        public DateTime? EmecFechaFinVigencia { get; set; }
        public int EmexCod { get; set; }
        public int VigeCod { get; set; }

        public virtual EmpresaExternas EmexCodNavigation { get; set; }
        public virtual Vigencias VigeCodNavigation { get; set; }
    }
}
