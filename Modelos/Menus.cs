﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Menus
    {
        public int MenuCod { get; set; }
        public string MenuNombre { get; set; }
        public int? MenuOrden { get; set; }
    }
}
