﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class CargoDescripciones
    {
        public int CadeCod { get; set; }
        public int? DescCod { get; set; }
        public int? CargCod { get; set; }
        public int? VigeCod { get; set; }

        public virtual Cargos CargCodNavigation { get; set; }
        public virtual Descripciones DescCodNavigation { get; set; }
        public virtual Vigencias VigeCodNavigation { get; set; }
    }
}
