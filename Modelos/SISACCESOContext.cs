﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ViasChileAuthAPI.Modelos
{
    //Scaffold-DbContext -Connection Name=SISACCESOQARZ  Microsoft.EntityFrameworkCore.SqlServer -OutputDir Modelos -Context "SISACCESOContext" 
    public partial class SISACCESOContext : DbContext
    {
        public SISACCESOContext()
        {
        }

        public SISACCESOContext(DbContextOptions<SISACCESOContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Administradores> Administradores { get; set; }
        public virtual DbSet<Atributos> Atributos { get; set; }
        public virtual DbSet<CargoDescripciones> CargoDescripciones { get; set; }
        public virtual DbSet<Cargos> Cargos { get; set; }
        public virtual DbSet<CategoriaAtributos> CategoriaAtributos { get; set; }
        public virtual DbSet<CentroCostos> CentroCostos { get; set; }
        public virtual DbSet<Comunas> Comunas { get; set; }
        public virtual DbSet<DependenciaNiveles> DependenciaNiveles { get; set; }
        public virtual DbSet<DependenciaRecursos> DependenciaRecursos { get; set; }
        public virtual DbSet<Descripciones> Descripciones { get; set; }
        public virtual DbSet<Emails> Emails { get; set; }
        public virtual DbSet<EmpresaExternaContactos> EmpresaExternaContactos { get; set; }
        public virtual DbSet<EmpresaExternaTipos> EmpresaExternaTipos { get; set; }
        public virtual DbSet<EmpresaExternas> EmpresaExternas { get; set; }
        public virtual DbSet<EmpresaNiveles> EmpresaNiveles { get; set; }
        public virtual DbSet<Empresas> Empresas { get; set; }
        public virtual DbSet<EstadoCiviles> EstadoCiviles { get; set; }
        public virtual DbSet<Fotografias> Fotografias { get; set; }
        public virtual DbSet<GrupoAtributos> GrupoAtributos { get; set; }
        public virtual DbSet<Menus> Menus { get; set; }
        public virtual DbSet<Modulos> Modulos { get; set; }
        public virtual DbSet<Niveles> Niveles { get; set; }
        public virtual DbSet<PerfilModulos> PerfilModulos { get; set; }
        public virtual DbSet<Perfilamientos> Perfilamientos { get; set; }
        public virtual DbSet<Perfiles> Perfiles { get; set; }
        public virtual DbSet<PersonaAtributos> PersonaAtributos { get; set; }
        public virtual DbSet<PersonaModos> PersonaModos { get; set; }
        public virtual DbSet<PersonaSistemas> PersonaSistemas { get; set; }
        public virtual DbSet<Personas> Personas { get; set; }
        public virtual DbSet<Provincias> Provincias { get; set; }
        public virtual DbSet<Recursos> Recursos { get; set; }
        public virtual DbSet<Regiones> Regiones { get; set; }
        public virtual DbSet<SexoTipos> SexoTipos { get; set; }
        public virtual DbSet<SistemaPerfiles> SistemaPerfiles { get; set; }
        public virtual DbSet<Sistemas> Sistemas { get; set; }
        public virtual DbSet<TipoAtributos> TipoAtributos { get; set; }
        public virtual DbSet<TipoCargos> TipoCargos { get; set; }
        public virtual DbSet<TipoNiveles> TipoNiveles { get; set; }
        public virtual DbSet<TipoPersonas> TipoPersonas { get; set; }
        public virtual DbSet<UsuarioEmpresaExternas> UsuarioEmpresaExternas { get; set; }
        public virtual DbSet<Usuarios> Usuarios { get; set; }
        public virtual DbSet<Vigencias> Vigencias { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Name=SISACCESOQARZ");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Administradores>(entity =>
            {
                entity.HasKey(e => e.AdmiCod);

                entity.ToTable("Administradores", "ADM");

                entity.Property(e => e.AdmiCod)
                    .HasColumnName("admi_cod")
                    .ValueGeneratedNever();

                entity.Property(e => e.AdmiEmail)
                    .HasColumnName("admi_email")
                    .HasMaxLength(200);

                entity.Property(e => e.AdmiNombre)
                    .IsRequired()
                    .HasColumnName("admi_nombre")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.AdmiNombreCompleto)
                    .HasColumnName("admi_nombre_completo")
                    .HasMaxLength(100);

                entity.Property(e => e.AdmiUltAcceso)
                    .HasColumnName("admi_ult_acceso")
                    .HasColumnType("datetime");

                entity.Property(e => e.PerfCod).HasColumnName("perf_cod");

                entity.Property(e => e.VigeCod)
                    .HasColumnName("vige_cod")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.PerfCodNavigation)
                    .WithMany(p => p.Administradores)
                    .HasForeignKey(d => d.PerfCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ADMINISTRADORES_REFERENCE_PERFILES");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.Administradores)
                    .HasForeignKey(d => d.VigeCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Administradores_Vigencia");
            });

            modelBuilder.Entity<Atributos>(entity =>
            {
                entity.HasKey(e => e.AtriCod)
                    .HasName("PK_ATRIBUTOS");

                entity.ToTable("Atributos", "FIC");

                entity.Property(e => e.AtriCod).HasColumnName("atri_cod");

                entity.Property(e => e.AtriApiLista)
                    .HasColumnName("atri_api_lista")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.AtriApiRegistro)
                    .HasColumnName("atri_api_registro")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.AtriMaximo).HasColumnName("atri_maximo");

                entity.Property(e => e.AtriNombre)
                    .HasColumnName("atri_nombre")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AtriOrden).HasColumnName("atri_orden");

                entity.Property(e => e.GratCod).HasColumnName("grat_cod");

                entity.Property(e => e.TiatCod).HasColumnName("tiat_cod");

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.GratCodNavigation)
                    .WithMany(p => p.Atributos)
                    .HasForeignKey(d => d.GratCod)
                    .HasConstraintName("FK_ATRIBUTO_REFERENCE_GRUPOSAT");

                entity.HasOne(d => d.TiatCodNavigation)
                    .WithMany(p => p.Atributos)
                    .HasForeignKey(d => d.TiatCod)
                    .HasConstraintName("FK_Atributos_TiposAtributos");
            });

            modelBuilder.Entity<CargoDescripciones>(entity =>
            {
                entity.HasKey(e => e.CadeCod)
                    .HasName("PK_CARGOSDESCRIPCIONES");

                entity.ToTable("Cargo_Descripciones", "ORG");

                entity.Property(e => e.CadeCod).HasColumnName("cade_cod");

                entity.Property(e => e.CargCod).HasColumnName("carg_cod");

                entity.Property(e => e.DescCod).HasColumnName("desc_cod");

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.CargCodNavigation)
                    .WithMany(p => p.CargoDescripciones)
                    .HasForeignKey(d => d.CargCod)
                    .HasConstraintName("FK_CARGOSDE_REFERENCE_CARGOS");

                entity.HasOne(d => d.DescCodNavigation)
                    .WithMany(p => p.CargoDescripciones)
                    .HasForeignKey(d => d.DescCod)
                    .HasConstraintName("FK_CARGOSDE_REFERENCE_DESCRIPC");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.CargoDescripciones)
                    .HasForeignKey(d => d.VigeCod)
                    .HasConstraintName("FK_CARGOSDE_REFERENCE_VIGENCIA");
            });

            modelBuilder.Entity<Cargos>(entity =>
            {
                entity.HasKey(e => e.CargCod)
                    .HasName("PK_CARGOS");

                entity.ToTable("Cargos", "ORG");

                entity.Property(e => e.CargCod).HasColumnName("carg_cod");

                entity.Property(e => e.CargNombre)
                    .HasColumnName("carg_nombre")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TicaCod).HasColumnName("tica_cod");

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.TicaCodNavigation)
                    .WithMany(p => p.Cargos)
                    .HasForeignKey(d => d.TicaCod)
                    .HasConstraintName("FK_CARGOS_REFERENCE_TIPOSCAR");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.Cargos)
                    .HasForeignKey(d => d.VigeCod)
                    .HasConstraintName("FK_CARGOS_REFERENCE_VIGENCIA");
            });

            modelBuilder.Entity<CategoriaAtributos>(entity =>
            {
                entity.HasKey(e => e.CaatCod)
                    .HasName("PK_CATEGORIASATRIBUTOS");

                entity.ToTable("Categoria_Atributos", "FIC");

                entity.Property(e => e.CaatCod).HasColumnName("caat_cod");

                entity.Property(e => e.CaatNombre)
                    .HasColumnName("caat_nombre")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CaatOrden).HasColumnName("caat_orden");

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");
            });

            modelBuilder.Entity<CentroCostos>(entity =>
            {
                entity.HasKey(e => e.CecoCod);

                entity.ToTable("Centro_Costos", "ORG");

                entity.Property(e => e.CecoCod).HasColumnName("ceco_cod");

                entity.Property(e => e.CecoNombre)
                    .HasColumnName("ceco_nombre")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");
            });

            modelBuilder.Entity<Comunas>(entity =>
            {
                entity.HasKey(e => e.ComuCod)
                    .HasName("PK_[Comunas");

                entity.ToTable("Comunas", "FIC");

                entity.Property(e => e.ComuCod)
                    .HasColumnName("comu_cod")
                    .ValueGeneratedNever();

                entity.Property(e => e.ComuNombre)
                    .IsRequired()
                    .HasColumnName("comu_nombre")
                    .HasMaxLength(100);

                entity.Property(e => e.ProvCod).HasColumnName("prov_cod");

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.ProvCodNavigation)
                    .WithMany(p => p.Comunas)
                    .HasForeignKey(d => d.ProvCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Comunas_Provincias");
            });

            modelBuilder.Entity<DependenciaNiveles>(entity =>
            {
                entity.HasKey(e => e.DeniCod)
                    .HasName("PK_DEPENDENCIASNIVELES");

                entity.ToTable("Dependencia_Niveles", "ORG");

                entity.Property(e => e.DeniCod).HasColumnName("deni_cod");

                entity.Property(e => e.EmniCod).HasColumnName("emni_cod");

                entity.Property(e => e.EmniCodSub).HasColumnName("emni_cod_sub");

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.EmniCodNavigation)
                    .WithMany(p => p.DependenciaNivelesEmniCodNavigation)
                    .HasForeignKey(d => d.EmniCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DependenciasNiveles_EmpresasNiveles");

                entity.HasOne(d => d.EmniCodSubNavigation)
                    .WithMany(p => p.DependenciaNivelesEmniCodSubNavigation)
                    .HasForeignKey(d => d.EmniCodSub)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DependenciasNiveles_EmpresasNiveles1");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.DependenciaNiveles)
                    .HasForeignKey(d => d.VigeCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DependenciasNiveles_Vigencia");
            });

            modelBuilder.Entity<DependenciaRecursos>(entity =>
            {
                entity.HasKey(e => e.DereCod)
                    .HasName("PK_DEPENDENCIASRECURSOS");

                entity.ToTable("Dependencia_Recursos", "ORG");

                entity.Property(e => e.DereCod).HasColumnName("dere_cod");

                entity.Property(e => e.DereResponsable).HasColumnName("dere_responsable");

                entity.Property(e => e.RecuCod).HasColumnName("recu_cod");

                entity.Property(e => e.RecuCodSub).HasColumnName("recu_cod_sub");

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.RecuCodNavigation)
                    .WithMany(p => p.DependenciaRecursosRecuCodNavigation)
                    .HasForeignKey(d => d.RecuCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DependenciasRecursos_Recursos");

                entity.HasOne(d => d.RecuCodSubNavigation)
                    .WithMany(p => p.DependenciaRecursosRecuCodSubNavigation)
                    .HasForeignKey(d => d.RecuCodSub)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DependenciasRecursos_Recursos1");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.DependenciaRecursos)
                    .HasForeignKey(d => d.VigeCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DependenciasRecursos_Vigencia");
            });

            modelBuilder.Entity<Descripciones>(entity =>
            {
                entity.HasKey(e => e.DescCod)
                    .HasName("PK_DESCRIPCIONES");

                entity.ToTable("Descripciones", "ORG");

                entity.Property(e => e.DescCod).HasColumnName("desc_cod");

                entity.Property(e => e.DescCodigo)
                    .HasColumnName("desc_codigo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DescDetalle)
                    .HasColumnName("desc_detalle")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.Descripciones)
                    .HasForeignKey(d => d.VigeCod)
                    .HasConstraintName("FK_DESCRIPC_REFERENCE_VIGENCIA");
            });

            modelBuilder.Entity<Emails>(entity =>
            {
                entity.HasKey(e => e.EmaiCod)
                    .HasName("PK_[Emails");

                entity.ToTable("Emails", "ADM");

                entity.Property(e => e.EmaiCod).HasColumnName("emai_cod");

                entity.Property(e => e.EmaiAsunto)
                    .IsRequired()
                    .HasColumnName("emai_asunto")
                    .HasMaxLength(255);

                entity.Property(e => e.EmaiMensaje)
                    .IsRequired()
                    .HasColumnName("emai_mensaje")
                    .HasColumnType("text");

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");
            });

            modelBuilder.Entity<EmpresaExternaContactos>(entity =>
            {
                entity.HasKey(e => e.EmecCod)
                    .HasName("PK_Empresa_Contactos");

                entity.ToTable("Empresa_Externa_Contactos", "EMP");

                entity.Property(e => e.EmecCod).HasColumnName("emec_cod");

                entity.Property(e => e.EmecCargo)
                    .HasColumnName("emec_cargo")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmecEmail)
                    .HasColumnName("emec_email")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmecFechaFinVigencia)
                    .HasColumnName("emec_fecha_fin_vigencia")
                    .HasColumnType("datetime");

                entity.Property(e => e.EmecFechaRegistro)
                    .HasColumnName("emec_fecha_registro")
                    .HasColumnType("datetime");

                entity.Property(e => e.EmecNombre)
                    .HasColumnName("emec_nombre")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmecTelefono)
                    .HasColumnName("emec_telefono")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EmexCod).HasColumnName("emex_cod");

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.EmexCodNavigation)
                    .WithMany(p => p.EmpresaExternaContactos)
                    .HasForeignKey(d => d.EmexCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Empresa_Contactos_Empresa_Externas");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.EmpresaExternaContactos)
                    .HasForeignKey(d => d.VigeCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Empresa_Contactos_Vigencia");
            });

            modelBuilder.Entity<EmpresaExternaTipos>(entity =>
            {
                entity.HasKey(e => e.EetiCod);

                entity.ToTable("Empresa_Externa_Tipos", "EMP");

                entity.Property(e => e.EetiCod).HasColumnName("eeti_cod");

                entity.Property(e => e.EetiNombre)
                    .IsRequired()
                    .HasColumnName("eeti_nombre")
                    .HasMaxLength(150);

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.EmpresaExternaTipos)
                    .HasForeignKey(d => d.VigeCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmpresaExternaTipos_Vigencia");
            });

            modelBuilder.Entity<EmpresaExternas>(entity =>
            {
                entity.HasKey(e => e.EmexCod);

                entity.ToTable("Empresa_Externas", "EMP");

                entity.Property(e => e.EmexCod).HasColumnName("emex_cod");

                entity.Property(e => e.ComuCod).HasColumnName("comu_cod");

                entity.Property(e => e.EetiCod).HasColumnName("eeti_cod");

                entity.Property(e => e.EmexDireccion)
                    .HasColumnName("emex_direccion")
                    .HasMaxLength(250);

                entity.Property(e => e.EmexDv)
                    .HasColumnName("emex_dv")
                    .HasMaxLength(1);

                entity.Property(e => e.EmexDvRepre)
                    .HasColumnName("emex_dv_repre")
                    .HasMaxLength(1);

                entity.Property(e => e.EmexEmail)
                    .HasColumnName("emex_email")
                    .HasMaxLength(100);

                entity.Property(e => e.EmexFechaInicioAct)
                    .HasColumnName("emex_fecha_inicio_act")
                    .HasColumnType("date");

                entity.Property(e => e.EmexFechaRegistro)
                    .HasColumnName("emex_fecha_registro")
                    .HasColumnType("date");

                entity.Property(e => e.EmexGiro)
                    .HasColumnName("emex_giro")
                    .HasMaxLength(150);

                entity.Property(e => e.EmexNombreCorto)
                    .HasColumnName("emex_nombre_corto")
                    .HasMaxLength(250);

                entity.Property(e => e.EmexNombreFantasia)
                    .HasColumnName("emex_nombre_fantasia")
                    .HasMaxLength(250);

                entity.Property(e => e.EmexNombreRepre)
                    .HasColumnName("emex_nombre_repre")
                    .HasMaxLength(100);

                entity.Property(e => e.EmexProvEstrategico).HasColumnName("emex_prov_estrategico");

                entity.Property(e => e.EmexRazonSocial)
                    .HasColumnName("emex_razon_social")
                    .HasMaxLength(250);

                entity.Property(e => e.EmexRut)
                    .HasColumnName("emex_rut")
                    .HasMaxLength(10);

                entity.Property(e => e.EmexRutRepre)
                    .HasColumnName("emex_rut_repre")
                    .HasMaxLength(10);

                entity.Property(e => e.EmexTelefono)
                    .HasColumnName("emex_telefono")
                    .HasMaxLength(50);

                entity.Property(e => e.EmexUltUpdate)
                    .HasColumnName("emex_ult_update")
                    .HasColumnType("datetime");

                entity.Property(e => e.ProvCod).HasColumnName("prov_cod");

                entity.Property(e => e.RegiCod).HasColumnName("regi_cod");

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.ComuCodNavigation)
                    .WithMany(p => p.EmpresaExternas)
                    .HasForeignKey(d => d.ComuCod)
                    .HasConstraintName("FK_Empresa_Externas_Comunas");

                entity.HasOne(d => d.EetiCodNavigation)
                    .WithMany(p => p.EmpresaExternas)
                    .HasForeignKey(d => d.EetiCod)
                    .HasConstraintName("FK_Empresa_Externas_Empresa_Externa_Tipos");

                entity.HasOne(d => d.ProvCodNavigation)
                    .WithMany(p => p.EmpresaExternas)
                    .HasForeignKey(d => d.ProvCod)
                    .HasConstraintName("FK_Empresa_Externas_Provincias");

                entity.HasOne(d => d.RegiCodNavigation)
                    .WithMany(p => p.EmpresaExternas)
                    .HasForeignKey(d => d.RegiCod)
                    .HasConstraintName("FK_Empresa_Externas_Regiones");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.EmpresaExternas)
                    .HasForeignKey(d => d.VigeCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Empresa_Externas_Vigencia");
            });

            modelBuilder.Entity<EmpresaNiveles>(entity =>
            {
                entity.HasKey(e => e.EmniCod)
                    .HasName("PK_EMPRESASNIVELES");

                entity.ToTable("Empresa_Niveles", "ORG");

                entity.HasIndex(e => new { e.NiveCod, e.EmprCod })
                    .HasName("IX_Empresa_Niveles")
                    .IsUnique();

                entity.Property(e => e.EmniCod).HasColumnName("emni_cod");

                entity.Property(e => e.CecoCod).HasColumnName("ceco_cod");

                entity.Property(e => e.EmniLeft)
                    .HasColumnName("emni_left")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmniTop)
                    .HasColumnName("emni_top")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmprCod).HasColumnName("empr_cod");

                entity.Property(e => e.NiveCod).HasColumnName("nive_cod");

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.CecoCodNavigation)
                    .WithMany(p => p.EmpresaNiveles)
                    .HasForeignKey(d => d.CecoCod)
                    .HasConstraintName("FK_Empresa_Niveles_Centro_Costos");

                entity.HasOne(d => d.EmprCodNavigation)
                    .WithMany(p => p.EmpresaNiveles)
                    .HasForeignKey(d => d.EmprCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EMPRESAS_REFERENCE_EMPRESAS");

                entity.HasOne(d => d.NiveCodNavigation)
                    .WithMany(p => p.EmpresaNiveles)
                    .HasForeignKey(d => d.NiveCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EMPRESAS_REFERENCE_NIVELES");
            });

            modelBuilder.Entity<Empresas>(entity =>
            {
                entity.HasKey(e => e.EmprCod)
                    .HasName("PK_EMPRESAS");

                entity.ToTable("Empresas", "ORG");

                entity.Property(e => e.EmprCod).HasColumnName("empr_cod");

                entity.Property(e => e.EmprAbreviatura)
                    .HasColumnName("empr_abreviatura")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmprLogo)
                    .HasColumnName("empr_logo")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.EmprNombre)
                    .HasColumnName("empr_nombre")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmprRazonSocial)
                    .HasColumnName("empr_razon_social")
                    .HasMaxLength(250);

                entity.Property(e => e.EmprRut)
                    .HasColumnName("empr_rut")
                    .HasMaxLength(20);

                entity.Property(e => e.EmprUltUpdate)
                    .HasColumnName("empr_ult_update")
                    .HasColumnType("datetime");

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.Empresas)
                    .HasForeignKey(d => d.VigeCod)
                    .HasConstraintName("FK_Empresas_Vigencia");
            });

            modelBuilder.Entity<EstadoCiviles>(entity =>
            {
                entity.HasKey(e => e.EsciCod)
                    .HasName("PK_ESTADOSCIVILES");

                entity.ToTable("Estado_Civiles", "FIC");

                entity.Property(e => e.EsciCod).HasColumnName("esci_cod");

                entity.Property(e => e.EsciNombre)
                    .HasColumnName("esci_nombre")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Fotografias>(entity =>
            {
                entity.HasKey(e => e.FotoCod);

                entity.ToTable("Fotografias", "FIC");

                entity.Property(e => e.FotoCod).HasColumnName("foto_cod");

                entity.Property(e => e.FotoDv)
                    .HasColumnName("foto_dv")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FotoImg).HasColumnName("foto_img");

                entity.Property(e => e.FotoRut)
                    .HasColumnName("foto_rut")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.PersCod).HasColumnName("pers_cod");
            });

            modelBuilder.Entity<GrupoAtributos>(entity =>
            {
                entity.HasKey(e => e.GratCod)
                    .HasName("PK_GRUPOSATRIBUTOS");

                entity.ToTable("Grupo_Atributos", "FIC");

                entity.Property(e => e.GratCod).HasColumnName("grat_cod");

                entity.Property(e => e.CaatCod).HasColumnName("caat_cod");

                entity.Property(e => e.GratNombre)
                    .HasColumnName("grat_nombre")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.GratOrden).HasColumnName("grat_orden");

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.CaatCodNavigation)
                    .WithMany(p => p.GrupoAtributos)
                    .HasForeignKey(d => d.CaatCod)
                    .HasConstraintName("FK_GruposAtributos_CategoriasAtributos");
            });

            modelBuilder.Entity<Menus>(entity =>
            {
                entity.HasKey(e => e.MenuCod)
                    .HasName("PK_Menu");

                entity.ToTable("Menus", "ADM");

                entity.Property(e => e.MenuCod)
                    .HasColumnName("menu_cod")
                    .ValueGeneratedNever();

                entity.Property(e => e.MenuNombre)
                    .IsRequired()
                    .HasColumnName("menu_nombre")
                    .HasMaxLength(100);

                entity.Property(e => e.MenuOrden).HasColumnName("menu_orden");
            });

            modelBuilder.Entity<Modulos>(entity =>
            {
                entity.HasKey(e => e.ModuCod);

                entity.ToTable("Modulos", "ADM");

                entity.Property(e => e.ModuCod).HasColumnName("modu_cod");

                entity.Property(e => e.MenuCod).HasColumnName("menu_cod");

                entity.Property(e => e.ModuAyuda)
                    .HasColumnName("modu_ayuda")
                    .HasColumnType("text");

                entity.Property(e => e.ModuCodPadre).HasColumnName("modu_cod_padre");

                entity.Property(e => e.ModuControlador)
                    .HasColumnName("modu_controlador")
                    .HasMaxLength(50);

                entity.Property(e => e.ModuNombre)
                    .IsRequired()
                    .HasColumnName("modu_nombre")
                    .HasMaxLength(100);

                entity.Property(e => e.ModuOrden).HasColumnName("modu_orden");

                entity.Property(e => e.ModuVista)
                    .HasColumnName("modu_vista")
                    .HasMaxLength(50);

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");
            });

            modelBuilder.Entity<Niveles>(entity =>
            {
                entity.HasKey(e => e.NiveCod)
                    .HasName("PK_NIVELES");

                entity.ToTable("Niveles", "ORG");

                entity.Property(e => e.NiveCod).HasColumnName("nive_cod");

                entity.Property(e => e.NiveNombre)
                    .HasColumnName("nive_nombre")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TiniCod).HasColumnName("tini_cod");

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.TiniCodNavigation)
                    .WithMany(p => p.Niveles)
                    .HasForeignKey(d => d.TiniCod)
                    .HasConstraintName("FK_NIVELES_REFERENCE_TIPOSNIV");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.Niveles)
                    .HasForeignKey(d => d.VigeCod)
                    .HasConstraintName("FK_NIVELES_REFERENCE_VIGENCIA");
            });

            modelBuilder.Entity<PerfilModulos>(entity =>
            {
                entity.HasKey(e => e.PemoCod);

                entity.ToTable("Perfil_Modulos", "ADM");

                entity.Property(e => e.PemoCod).HasColumnName("pemo_cod");

                entity.Property(e => e.ModuCod).HasColumnName("modu_cod");

                entity.Property(e => e.PerfCod).HasColumnName("perf_cod");

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.ModuCodNavigation)
                    .WithMany(p => p.PerfilModulos)
                    .HasForeignKey(d => d.ModuCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Perfil_Modulos_Modulos");

                entity.HasOne(d => d.PerfCodNavigation)
                    .WithMany(p => p.PerfilModulos)
                    .HasForeignKey(d => d.PerfCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Perfil_Modulos_Perfiles");
            });

            modelBuilder.Entity<Perfilamientos>(entity =>
            {
                entity.HasKey(e => e.PerfCod)
                    .HasName("PK_PERFILES");

                entity.ToTable("Perfilamientos", "ACC");

                entity.Property(e => e.PerfCod)
                    .HasColumnName("perf_cod")
                    .ValueGeneratedNever();

                entity.Property(e => e.PerfDescripcion)
                    .HasColumnName("perf_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.PerfNombre)
                    .IsRequired()
                    .HasColumnName("perf_nombre")
                    .HasMaxLength(250);

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.Perfilamientos)
                    .HasForeignKey(d => d.VigeCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Perfilamientos_Vigencias");
            });

            modelBuilder.Entity<Perfiles>(entity =>
            {
                entity.HasKey(e => e.PerfCod)
                    .HasName("PK_PERFILES");

                entity.ToTable("Perfiles", "ADM");

                entity.Property(e => e.PerfCod)
                    .HasColumnName("perf_cod")
                    .ValueGeneratedNever();

                entity.Property(e => e.PerfDescripcion)
                    .HasColumnName("perf_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.PerfNombre)
                    .IsRequired()
                    .HasColumnName("perf_nombre")
                    .HasMaxLength(250);

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.Perfiles)
                    .HasForeignKey(d => d.VigeCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Perfiles_Vigencias");
            });

            modelBuilder.Entity<PersonaAtributos>(entity =>
            {
                entity.HasKey(e => e.PeatCod)
                    .HasName("PK_PERSONASATRIBUTOS");

                entity.ToTable("Persona_Atributos", "FIC");

                entity.Property(e => e.PeatCod).HasColumnName("peat_cod");

                entity.Property(e => e.AtriCod).HasColumnName("atri_cod");

                entity.Property(e => e.PeatValor)
                    .HasColumnName("peat_valor")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PersCod).HasColumnName("pers_cod");

                entity.HasOne(d => d.AtriCodNavigation)
                    .WithMany(p => p.PersonaAtributos)
                    .HasForeignKey(d => d.AtriCod)
                    .HasConstraintName("FK_PERSONAS_REFERENCE_ATRIBUTO");

                entity.HasOne(d => d.PersCodNavigation)
                    .WithMany(p => p.PersonaAtributos)
                    .HasForeignKey(d => d.PersCod)
                    .HasConstraintName("FK_PERSONAS_REFERENCE_PERSONAS");
            });

            modelBuilder.Entity<PersonaModos>(entity =>
            {
                entity.HasKey(e => e.PemoCod)
                    .HasName("PK_Usuario_Modo");

                entity.ToTable("Persona_Modos", "ACC");

                entity.Property(e => e.PemoCod).HasColumnName("pemo_cod");

                entity.Property(e => e.PemoModo)
                    .HasColumnName("pemo_modo")
                    .HasMaxLength(25);

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.PersonaModos)
                    .HasForeignKey(d => d.VigeCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Persona_Modos_Vigencias");
            });

            modelBuilder.Entity<PersonaSistemas>(entity =>
            {
                entity.HasKey(e => e.PeisCod)
                    .HasName("PK_ACC.Usuario_Sistemas");

                entity.ToTable("Persona_Sistemas", "ACC");

                entity.Property(e => e.PeisCod).HasColumnName("peis_cod");

                entity.Property(e => e.PeisUltUpdate)
                    .HasColumnName("peis_ult_update")
                    .HasColumnType("datetime");

                entity.Property(e => e.PersCod).HasColumnName("pers_cod");

                entity.Property(e => e.SipeCod).HasColumnName("sipe_cod");

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.PersCodNavigation)
                    .WithMany(p => p.PersonaSistemas)
                    .HasForeignKey(d => d.PersCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Persona_Sistemas_Personas");

                entity.HasOne(d => d.SipeCodNavigation)
                    .WithMany(p => p.PersonaSistemas)
                    .HasForeignKey(d => d.SipeCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Persona_Sistemas_Sistemas_Perfiles");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.PersonaSistemas)
                    .HasForeignKey(d => d.VigeCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Usuario_Sistemas_Vigencia");
            });

            modelBuilder.Entity<Personas>(entity =>
            {
                entity.HasKey(e => e.PersCod)
                    .HasName("PK_PERSONAS");

                entity.ToTable("Personas", "FIC");

                entity.Property(e => e.PersCod)
                    .HasColumnName("pers_cod")
                    .ValueGeneratedNever();

                entity.Property(e => e.CecoCodPers).HasColumnName("ceco_cod_pers");

                entity.Property(e => e.EsciCod).HasColumnName("esci_cod");

                entity.Property(e => e.PemoCod)
                    .HasColumnName("pemo_cod")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.PersApMaterno)
                    .HasColumnName("pers_ap_materno")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PersApPaterno)
                    .HasColumnName("pers_ap_paterno")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PersCambioPass)
                    .HasColumnName("pers_cambio_pass")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PersCodAux)
                    .HasColumnName("pers_cod_aux")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PersDv)
                    .HasColumnName("pers_dv")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PersEmail)
                    .HasColumnName("pers_email")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PersFechaNacimiento)
                    .HasColumnName("pers_fecha_nacimiento")
                    .HasColumnType("datetime");

                entity.Property(e => e.PersNombreCompleto)
                    .IsRequired()
                    .HasColumnName("pers_nombre_completo")
                    .HasMaxLength(767)
                    .IsUnicode(false)
                    .HasComputedColumnSql("((((isnull([pers_nombres],'')+' ')+isnull([pers_ap_paterno],''))+' ')+isnull([pers_ap_materno],''))");

                entity.Property(e => e.PersNombres)
                    .HasColumnName("pers_nombres")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PersPassword)
                    .HasColumnName("pers_password")
                    .HasMaxLength(100);

                entity.Property(e => e.PersRut)
                    .HasColumnName("pers_rut")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.PersStaff).HasColumnName("pers_staff");

                entity.Property(e => e.PersUsername)
                    .HasColumnName("pers_username")
                    .HasMaxLength(100);

                entity.Property(e => e.SetiCod).HasColumnName("seti_cod");

                entity.Property(e => e.TipeCod).HasColumnName("tipe_cod");

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.PemoCodNavigation)
                    .WithMany(p => p.Personas)
                    .HasForeignKey(d => d.PemoCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Personas_Persona_Modos");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.Personas)
                    .HasForeignKey(d => d.VigeCod)
                    .HasConstraintName("FK_Personas_Vigencias");
            });

            modelBuilder.Entity<Provincias>(entity =>
            {
                entity.HasKey(e => e.ProvCod)
                    .HasName("PK_[Provincias");

                entity.ToTable("Provincias", "FIC");

                entity.Property(e => e.ProvCod)
                    .HasColumnName("prov_cod")
                    .ValueGeneratedNever();

                entity.Property(e => e.ProvNombre)
                    .IsRequired()
                    .HasColumnName("prov_nombre")
                    .HasMaxLength(100);

                entity.Property(e => e.RegiCod).HasColumnName("regi_cod");

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.RegiCodNavigation)
                    .WithMany(p => p.Provincias)
                    .HasForeignKey(d => d.RegiCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Provincias_Regiones");
            });

            modelBuilder.Entity<Recursos>(entity =>
            {
                entity.HasKey(e => e.RecuCod)
                    .HasName("PK_RECURSOS");

                entity.ToTable("Recursos", "ORG");

                entity.Property(e => e.RecuCod).HasColumnName("recu_cod");

                entity.Property(e => e.CargCod).HasColumnName("carg_cod");

                entity.Property(e => e.EmniCod).HasColumnName("emni_cod");

                entity.Property(e => e.JefeCod)
                    .HasColumnName("jefe_cod")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PayrCod)
                    .HasColumnName("payr_cod")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PersCod).HasColumnName("pers_cod");

                entity.Property(e => e.RecuLeft)
                    .HasColumnName("recu_left")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecuResponsable).HasColumnName("recu_responsable");

                entity.Property(e => e.RecuStaff)
                    .HasColumnName("recu_staff")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RecuTop)
                    .HasColumnName("recu_top")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoPers)
                    .HasColumnName("tipo_pers")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.CargCodNavigation)
                    .WithMany(p => p.Recursos)
                    .HasForeignKey(d => d.CargCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RECURSOS_REFERENCE_CARGOS");

                entity.HasOne(d => d.EmniCodNavigation)
                    .WithMany(p => p.Recursos)
                    .HasForeignKey(d => d.EmniCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RECURSOS_REFERENCE_EMPRESAS");

                entity.HasOne(d => d.PersCodNavigation)
                    .WithMany(p => p.Recursos)
                    .HasForeignKey(d => d.PersCod)
                    .HasConstraintName("FK_Recursos_Personas");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.Recursos)
                    .HasForeignKey(d => d.VigeCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RECURSOS_REFERENCE_VIGENCIA");
            });

            modelBuilder.Entity<Regiones>(entity =>
            {
                entity.HasKey(e => e.RegiCod)
                    .HasName("PK_[Regiones");

                entity.ToTable("Regiones", "FIC");

                entity.Property(e => e.RegiCod)
                    .HasColumnName("regi_cod")
                    .ValueGeneratedNever();

                entity.Property(e => e.RegiNombre)
                    .IsRequired()
                    .HasColumnName("regi_nombre")
                    .HasMaxLength(100);

                entity.Property(e => e.RegiNumero)
                    .HasColumnName("regi_numero")
                    .HasMaxLength(5);

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");
            });

            modelBuilder.Entity<SexoTipos>(entity =>
            {
                entity.HasKey(e => e.SetiCod)
                    .HasName("PK_SexoTipos");

                entity.ToTable("Sexo_Tipos", "FIC");

                entity.Property(e => e.SetiCod)
                    .HasColumnName("seti_cod")
                    .ValueGeneratedNever();

                entity.Property(e => e.SetiNombre)
                    .IsRequired()
                    .HasColumnName("seti_nombre")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SistemaPerfiles>(entity =>
            {
                entity.HasKey(e => e.SipeCod)
                    .HasName("PK_Sistemas_Perfiles");

                entity.ToTable("Sistema_Perfiles", "ACC");

                entity.Property(e => e.SipeCod).HasColumnName("sipe_cod");

                entity.Property(e => e.PerfCod).HasColumnName("perf_cod");

                entity.Property(e => e.SipeUltUpdate)
                    .HasColumnName("sipe_ult_update")
                    .HasColumnType("datetime");

                entity.Property(e => e.SistCod).HasColumnName("sist_cod");

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.PerfCodNavigation)
                    .WithMany(p => p.SistemaPerfiles)
                    .HasForeignKey(d => d.PerfCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Sistema_Perfiles_Perfilamientos");

                entity.HasOne(d => d.SistCodNavigation)
                    .WithMany(p => p.SistemaPerfiles)
                    .HasForeignKey(d => d.SistCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Sistemas_Perfiles_Sistemas");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.SistemaPerfiles)
                    .HasForeignKey(d => d.VigeCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Sistema_Perfiles_Vigencias");
            });

            modelBuilder.Entity<Sistemas>(entity =>
            {
                entity.HasKey(e => e.SistCod);

                entity.ToTable("Sistemas", "ACC");

                entity.Property(e => e.SistCod).HasColumnName("sist_cod");

                entity.Property(e => e.IdProveedor).HasColumnName("id_proveedor");

                entity.Property(e => e.IdUsuarioEncargado).HasColumnName("id_usuario_encargado");

                entity.Property(e => e.SistBbdd)
                    .HasColumnName("sist_bbdd")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.SistConexion)
                    .HasColumnName("sist_conexion")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SistFechaDesde)
                    .HasColumnName("sist_fecha_desde")
                    .HasColumnType("datetime");

                entity.Property(e => e.SistFechaHasta)
                    .HasColumnName("sist_fecha_hasta")
                    .HasColumnType("datetime");

                entity.Property(e => e.SistIcono).HasColumnName("sist_icono");

                entity.Property(e => e.SistLengDesa)
                    .HasColumnName("sist_leng_desa")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.SistNombre)
                    .HasColumnName("sist_nombre")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.SistPathAcceso)
                    .HasColumnName("sist_path_acceso")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.SistProveedor)
                    .HasColumnName("sist_proveedor")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.SistUrlRaiz)
                    .HasColumnName("sist_url_raiz")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.Sistemas)
                    .HasForeignKey(d => d.VigeCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Sistemas_Vigencia");
            });

            modelBuilder.Entity<TipoAtributos>(entity =>
            {
                entity.HasKey(e => e.TiatCod)
                    .HasName("PK_TiposAtributos");

                entity.ToTable("Tipo_Atributos", "FIC");

                entity.Property(e => e.TiatCod).HasColumnName("tiat_cod");

                entity.Property(e => e.TiatNombre)
                    .HasColumnName("tiat_nombre")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.TipoAtributos)
                    .HasForeignKey(d => d.VigeCod)
                    .HasConstraintName("FK_TiposAtributos_Vigencia");
            });

            modelBuilder.Entity<TipoCargos>(entity =>
            {
                entity.HasKey(e => e.TicaCod)
                    .HasName("PK_TIPOSCARGOS");

                entity.ToTable("Tipo_Cargos", "ORG");

                entity.Property(e => e.TicaCod).HasColumnName("tica_cod");

                entity.Property(e => e.TicaNombre)
                    .HasColumnName("tica_nombre")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");
            });

            modelBuilder.Entity<TipoNiveles>(entity =>
            {
                entity.HasKey(e => e.TiniCod)
                    .HasName("PK_TIPOSNIVELES");

                entity.ToTable("Tipo_Niveles", "ORG");

                entity.Property(e => e.TiniCod).HasColumnName("tini_cod");

                entity.Property(e => e.TiniNombre)
                    .HasColumnName("tini_nombre")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.TipoNiveles)
                    .HasForeignKey(d => d.VigeCod)
                    .HasConstraintName("FK_TIPOSNIV_REFERENCE_VIGENCIA");
            });

            modelBuilder.Entity<TipoPersonas>(entity =>
            {
                entity.HasKey(e => e.TipeCod)
                    .HasName("PK_TIPOSPERSONAS");

                entity.ToTable("Tipo_Personas", "FIC");

                entity.Property(e => e.TipeCod).HasColumnName("tipe_cod");

                entity.Property(e => e.TipeNombre)
                    .HasColumnName("tipe_nombre")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UsuarioEmpresaExternas>(entity =>
            {
                entity.HasKey(e => e.UseeCod)
                    .HasName("PK_Usuario_Empresa_Externa");

                entity.ToTable("Usuario_Empresa_Externas", "ACC");

                entity.Property(e => e.UseeCod).HasColumnName("usee_cod");

                entity.Property(e => e.CtacCod).HasColumnName("ctac_cod");

                entity.Property(e => e.EecoCod).HasColumnName("eeco_cod");

                entity.Property(e => e.UseeContrasena)
                    .IsRequired()
                    .HasColumnName("usee_contrasena")
                    .HasMaxLength(50);

                entity.Property(e => e.UseeEmail)
                    .HasColumnName("usee_email")
                    .HasMaxLength(100);

                entity.Property(e => e.UseeUltConexion)
                    .HasColumnName("usee_ult_conexion")
                    .HasColumnType("datetime");

                entity.Property(e => e.UseeUsuario)
                    .IsRequired()
                    .HasColumnName("usee_usuario")
                    .HasMaxLength(12);

                entity.Property(e => e.VigeCod).HasColumnName("vige_cod");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.UsuarioEmpresaExternas)
                    .HasForeignKey(d => d.VigeCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Usuario_Empresa_Externa_Vigencias");
            });

            modelBuilder.Entity<Usuarios>(entity =>
            {
                entity.HasKey(e => e.UsuaCod);

                entity.ToTable("Usuarios", "ADM");

                entity.HasComment("Tabla mantenedor de usuarios");

                entity.Property(e => e.UsuaCod)
                    .HasColumnName("usua_cod")
                    .HasComment("Código identificador de la tabla Usuarios")
                    .ValueGeneratedNever();

                entity.Property(e => e.EmprCod).HasColumnName("empr_cod");

                entity.Property(e => e.PerfCod)
                    .HasColumnName("perf_cod")
                    .HasDefaultValueSql("((5))")
                    .HasComment("Código que hace referencia a la tabla Perfiles");

                entity.Property(e => e.UsuaCargo)
                    .HasColumnName("usua_cargo")
                    .HasMaxLength(50);

                entity.Property(e => e.UsuaCookie)
                    .HasColumnName("usua_cookie")
                    .HasMaxLength(50);

                entity.Property(e => e.UsuaEmail)
                    .HasColumnName("usua_email")
                    .HasMaxLength(200)
                    .HasComment("Email del usuario");

                entity.Property(e => e.UsuaNombre)
                    .IsRequired()
                    .HasColumnName("usua_nombre")
                    .HasMaxLength(50)
                    .HasComment("Nombre del usuario");

                entity.Property(e => e.UsuaNombreCompleto)
                    .HasColumnName("usua_nombre_completo")
                    .HasMaxLength(100)
                    .HasComment("Nombre completo del usuario");

                entity.Property(e => e.UsuaSesion)
                    .HasColumnName("usua_sesion")
                    .HasMaxLength(50);

                entity.Property(e => e.UsuaUltAcceso)
                    .HasColumnName("usua_ult_acceso")
                    .HasColumnType("datetime")
                    .HasComment("Fecha del último acceso del usuario");

                entity.Property(e => e.VigeCod)
                    .HasColumnName("vige_cod")
                    .HasDefaultValueSql("((1))")
                    .HasComment("Código que hace referencia a la tabla Vigencia");

                entity.HasOne(d => d.EmprCodNavigation)
                    .WithMany(p => p.Usuarios)
                    .HasForeignKey(d => d.EmprCod)
                    .HasConstraintName("FK_Usuarios_Empresas");

                entity.HasOne(d => d.PerfCodNavigation)
                    .WithMany(p => p.Usuarios)
                    .HasForeignKey(d => d.PerfCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_USUARIOS_REFERENCE_PERFILES");

                entity.HasOne(d => d.VigeCodNavigation)
                    .WithMany(p => p.Usuarios)
                    .HasForeignKey(d => d.VigeCod)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_USUARIOS_REFERENCE_VIGENCIA");
            });

            modelBuilder.Entity<Vigencias>(entity =>
            {
                entity.HasKey(e => e.VigeCod)
                    .HasName("PK_VIGENCIA");

                entity.ToTable("Vigencias", "ADM");

                entity.Property(e => e.VigeCod)
                    .HasColumnName("vige_cod")
                    .ValueGeneratedNever();

                entity.Property(e => e.VigeNombre)
                    .HasColumnName("vige_nombre")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
