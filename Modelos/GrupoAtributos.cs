﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class GrupoAtributos
    {
        public GrupoAtributos()
        {
            Atributos = new HashSet<Atributos>();
        }

        public int GratCod { get; set; }
        public int? CaatCod { get; set; }
        public string GratNombre { get; set; }
        public int? GratOrden { get; set; }
        public int? VigeCod { get; set; }

        public virtual CategoriaAtributos CaatCodNavigation { get; set; }
        public virtual ICollection<Atributos> Atributos { get; set; }
    }
}
