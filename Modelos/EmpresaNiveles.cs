﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class EmpresaNiveles
    {
        public EmpresaNiveles()
        {
            DependenciaNivelesEmniCodNavigation = new HashSet<DependenciaNiveles>();
            DependenciaNivelesEmniCodSubNavigation = new HashSet<DependenciaNiveles>();
            Recursos = new HashSet<Recursos>();
        }

        public int EmniCod { get; set; }
        public int NiveCod { get; set; }
        public int EmprCod { get; set; }
        public int VigeCod { get; set; }
        public string EmniTop { get; set; }
        public string EmniLeft { get; set; }
        public int? CecoCod { get; set; }

        public virtual CentroCostos CecoCodNavigation { get; set; }
        public virtual Empresas EmprCodNavigation { get; set; }
        public virtual Niveles NiveCodNavigation { get; set; }
        public virtual ICollection<DependenciaNiveles> DependenciaNivelesEmniCodNavigation { get; set; }
        public virtual ICollection<DependenciaNiveles> DependenciaNivelesEmniCodSubNavigation { get; set; }
        public virtual ICollection<Recursos> Recursos { get; set; }
    }
}
