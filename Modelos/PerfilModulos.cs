﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class PerfilModulos
    {
        public int PemoCod { get; set; }
        public int PerfCod { get; set; }
        public int ModuCod { get; set; }
        public int VigeCod { get; set; }

        public virtual Modulos ModuCodNavigation { get; set; }
        public virtual Perfiles PerfCodNavigation { get; set; }
    }
}
