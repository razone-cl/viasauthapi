﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class TipoCargos
    {
        public TipoCargos()
        {
            Cargos = new HashSet<Cargos>();
        }

        public int TicaCod { get; set; }
        public string TicaNombre { get; set; }
        public int? VigeCod { get; set; }

        public virtual ICollection<Cargos> Cargos { get; set; }
    }
}
