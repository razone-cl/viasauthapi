﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class SexoTipos
    {
        public int SetiCod { get; set; }
        public string SetiNombre { get; set; }
    }
}
