﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class EmpresaExternas
    {
        public EmpresaExternas()
        {
            EmpresaExternaContactos = new HashSet<EmpresaExternaContactos>();
        }

        public int EmexCod { get; set; }
        public string EmexRazonSocial { get; set; }
        public string EmexNombreFantasia { get; set; }
        public string EmexNombreCorto { get; set; }
        public string EmexRut { get; set; }
        public string EmexDv { get; set; }
        public string EmexGiro { get; set; }
        public DateTime? EmexFechaInicioAct { get; set; }
        public string EmexRutRepre { get; set; }
        public string EmexDvRepre { get; set; }
        public string EmexNombreRepre { get; set; }
        public DateTime? EmexFechaRegistro { get; set; }
        public string EmexDireccion { get; set; }
        public string EmexTelefono { get; set; }
        public string EmexEmail { get; set; }
        public bool? EmexProvEstrategico { get; set; }
        public DateTime? EmexUltUpdate { get; set; }
        public int? EetiCod { get; set; }
        public int VigeCod { get; set; }
        public int? RegiCod { get; set; }
        public int? ProvCod { get; set; }
        public int? ComuCod { get; set; }

        public virtual Comunas ComuCodNavigation { get; set; }
        public virtual EmpresaExternaTipos EetiCodNavigation { get; set; }
        public virtual Provincias ProvCodNavigation { get; set; }
        public virtual Regiones RegiCodNavigation { get; set; }
        public virtual Vigencias VigeCodNavigation { get; set; }
        public virtual ICollection<EmpresaExternaContactos> EmpresaExternaContactos { get; set; }
    }
}
