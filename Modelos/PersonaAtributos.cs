﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class PersonaAtributos
    {
        public int PeatCod { get; set; }
        public int? PersCod { get; set; }
        public int? AtriCod { get; set; }
        public string PeatValor { get; set; }

        public virtual Atributos AtriCodNavigation { get; set; }
        public virtual Personas PersCodNavigation { get; set; }
    }
}
