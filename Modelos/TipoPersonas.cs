﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class TipoPersonas
    {
        public int TipeCod { get; set; }
        public string TipeNombre { get; set; }
    }
}
