﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Administradores
    {
        public int AdmiCod { get; set; }
        public string AdmiNombre { get; set; }
        public int PerfCod { get; set; }
        public DateTime? AdmiUltAcceso { get; set; }
        public int VigeCod { get; set; }
        public string AdmiNombreCompleto { get; set; }
        public string AdmiEmail { get; set; }

        public virtual Perfiles PerfCodNavigation { get; set; }
        public virtual Vigencias VigeCodNavigation { get; set; }
    }
}
