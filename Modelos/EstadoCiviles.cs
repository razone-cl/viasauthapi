﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class EstadoCiviles
    {
        public int EsciCod { get; set; }
        public string EsciNombre { get; set; }
    }
}
