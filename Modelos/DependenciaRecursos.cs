﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class DependenciaRecursos
    {
        public int DereCod { get; set; }
        public bool? DereResponsable { get; set; }
        public int RecuCod { get; set; }
        public int RecuCodSub { get; set; }
        public int VigeCod { get; set; }

        public virtual Recursos RecuCodNavigation { get; set; }
        public virtual Recursos RecuCodSubNavigation { get; set; }
        public virtual Vigencias VigeCodNavigation { get; set; }
    }
}
