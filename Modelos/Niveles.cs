﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Niveles
    {
        public Niveles()
        {
            EmpresaNiveles = new HashSet<EmpresaNiveles>();
        }

        public int NiveCod { get; set; }
        public int? TiniCod { get; set; }
        public int? VigeCod { get; set; }
        public string NiveNombre { get; set; }

        public virtual TipoNiveles TiniCodNavigation { get; set; }
        public virtual Vigencias VigeCodNavigation { get; set; }
        public virtual ICollection<EmpresaNiveles> EmpresaNiveles { get; set; }
    }
}
