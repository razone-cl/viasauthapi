﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class UsuarioEmpresaExternas
    {
        public int UseeCod { get; set; }
        public string UseeUsuario { get; set; }
        public string UseeContrasena { get; set; }
        public string UseeEmail { get; set; }
        public int EecoCod { get; set; }
        public DateTime? UseeUltConexion { get; set; }
        public int VigeCod { get; set; }
        public int? CtacCod { get; set; }

        public virtual Vigencias VigeCodNavigation { get; set; }
    }
}
