﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Emails
    {
        public int EmaiCod { get; set; }
        public string EmaiAsunto { get; set; }
        public string EmaiMensaje { get; set; }
        public int VigeCod { get; set; }
    }
}
