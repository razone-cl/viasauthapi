﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Sistemas
    {
        public Sistemas()
        {
            SistemaPerfiles = new HashSet<SistemaPerfiles>();
        }

        public int SistCod { get; set; }
        public int IdProveedor { get; set; }
        public int? IdUsuarioEncargado { get; set; }
        public string SistNombre { get; set; }
        public byte[] SistIcono { get; set; }
        public string SistPathAcceso { get; set; }
        public string SistUrlRaiz { get; set; }
        public int VigeCod { get; set; }
        public string SistBbdd { get; set; }
        public string SistLengDesa { get; set; }
        public DateTime? SistFechaDesde { get; set; }
        public DateTime? SistFechaHasta { get; set; }
        public string SistProveedor { get; set; }
        public string SistConexion { get; set; }

        public virtual Vigencias VigeCodNavigation { get; set; }
        public virtual ICollection<SistemaPerfiles> SistemaPerfiles { get; set; }
    }
}
