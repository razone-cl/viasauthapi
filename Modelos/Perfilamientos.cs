﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Perfilamientos
    {
        public Perfilamientos()
        {
            SistemaPerfiles = new HashSet<SistemaPerfiles>();
        }

        public int PerfCod { get; set; }
        public string PerfNombre { get; set; }
        public string PerfDescripcion { get; set; }
        public int VigeCod { get; set; }

        public virtual Vigencias VigeCodNavigation { get; set; }
        public virtual ICollection<SistemaPerfiles> SistemaPerfiles { get; set; }
    }
}
