﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class TipoNiveles
    {
        public TipoNiveles()
        {
            Niveles = new HashSet<Niveles>();
        }

        public int TiniCod { get; set; }
        public int? VigeCod { get; set; }
        public string TiniNombre { get; set; }

        public virtual Vigencias VigeCodNavigation { get; set; }
        public virtual ICollection<Niveles> Niveles { get; set; }
    }
}
