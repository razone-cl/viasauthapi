﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Usuarios
    {
        public int UsuaCod { get; set; }
        public string UsuaNombre { get; set; }
        public string UsuaNombreCompleto { get; set; }
        public DateTime? UsuaUltAcceso { get; set; }
        public string UsuaEmail { get; set; }
        public string UsuaCookie { get; set; }
        public string UsuaSesion { get; set; }
        public string UsuaCargo { get; set; }
        public int PerfCod { get; set; }
        public int VigeCod { get; set; }
        public int? EmprCod { get; set; }

        public virtual Empresas EmprCodNavigation { get; set; }
        public virtual Perfiles PerfCodNavigation { get; set; }
        public virtual Vigencias VigeCodNavigation { get; set; }
    }
}
