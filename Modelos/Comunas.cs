﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Comunas
    {
        public Comunas()
        {
            EmpresaExternas = new HashSet<EmpresaExternas>();
        }

        public int ComuCod { get; set; }
        public string ComuNombre { get; set; }
        public int ProvCod { get; set; }
        public int VigeCod { get; set; }

        public virtual Provincias ProvCodNavigation { get; set; }
        public virtual ICollection<EmpresaExternas> EmpresaExternas { get; set; }
    }
}
