﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class Vigencias
    {
        public Vigencias()
        {
            Administradores = new HashSet<Administradores>();
            CargoDescripciones = new HashSet<CargoDescripciones>();
            Cargos = new HashSet<Cargos>();
            DependenciaNiveles = new HashSet<DependenciaNiveles>();
            DependenciaRecursos = new HashSet<DependenciaRecursos>();
            Descripciones = new HashSet<Descripciones>();
            EmpresaExternaContactos = new HashSet<EmpresaExternaContactos>();
            EmpresaExternaTipos = new HashSet<EmpresaExternaTipos>();
            EmpresaExternas = new HashSet<EmpresaExternas>();
            Empresas = new HashSet<Empresas>();
            Niveles = new HashSet<Niveles>();
            Perfilamientos = new HashSet<Perfilamientos>();
            Perfiles = new HashSet<Perfiles>();
            PersonaModos = new HashSet<PersonaModos>();
            PersonaSistemas = new HashSet<PersonaSistemas>();
            Personas = new HashSet<Personas>();
            Recursos = new HashSet<Recursos>();
            SistemaPerfiles = new HashSet<SistemaPerfiles>();
            Sistemas = new HashSet<Sistemas>();
            TipoAtributos = new HashSet<TipoAtributos>();
            TipoNiveles = new HashSet<TipoNiveles>();
            UsuarioEmpresaExternas = new HashSet<UsuarioEmpresaExternas>();
            Usuarios = new HashSet<Usuarios>();
        }

        public int VigeCod { get; set; }
        public string VigeNombre { get; set; }

        public virtual ICollection<Administradores> Administradores { get; set; }
        public virtual ICollection<CargoDescripciones> CargoDescripciones { get; set; }
        public virtual ICollection<Cargos> Cargos { get; set; }
        public virtual ICollection<DependenciaNiveles> DependenciaNiveles { get; set; }
        public virtual ICollection<DependenciaRecursos> DependenciaRecursos { get; set; }
        public virtual ICollection<Descripciones> Descripciones { get; set; }
        public virtual ICollection<EmpresaExternaContactos> EmpresaExternaContactos { get; set; }
        public virtual ICollection<EmpresaExternaTipos> EmpresaExternaTipos { get; set; }
        public virtual ICollection<EmpresaExternas> EmpresaExternas { get; set; }
        public virtual ICollection<Empresas> Empresas { get; set; }
        public virtual ICollection<Niveles> Niveles { get; set; }
        public virtual ICollection<Perfilamientos> Perfilamientos { get; set; }
        public virtual ICollection<Perfiles> Perfiles { get; set; }
        public virtual ICollection<PersonaModos> PersonaModos { get; set; }
        public virtual ICollection<PersonaSistemas> PersonaSistemas { get; set; }
        public virtual ICollection<Personas> Personas { get; set; }
        public virtual ICollection<Recursos> Recursos { get; set; }
        public virtual ICollection<SistemaPerfiles> SistemaPerfiles { get; set; }
        public virtual ICollection<Sistemas> Sistemas { get; set; }
        public virtual ICollection<TipoAtributos> TipoAtributos { get; set; }
        public virtual ICollection<TipoNiveles> TipoNiveles { get; set; }
        public virtual ICollection<UsuarioEmpresaExternas> UsuarioEmpresaExternas { get; set; }
        public virtual ICollection<Usuarios> Usuarios { get; set; }
    }
}
