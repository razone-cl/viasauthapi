﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class DependenciaNiveles
    {
        public int DeniCod { get; set; }
        public int EmniCod { get; set; }
        public int EmniCodSub { get; set; }
        public int VigeCod { get; set; }

        public virtual EmpresaNiveles EmniCodNavigation { get; set; }
        public virtual EmpresaNiveles EmniCodSubNavigation { get; set; }
        public virtual Vigencias VigeCodNavigation { get; set; }
    }
}
