﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class PersonaModos
    {
        public PersonaModos()
        {
            Personas = new HashSet<Personas>();
        }

        public int PemoCod { get; set; }
        public string PemoModo { get; set; }
        public int VigeCod { get; set; }

        public virtual Vigencias VigeCodNavigation { get; set; }
        public virtual ICollection<Personas> Personas { get; set; }
    }
}
