﻿using System;
using System.Collections.Generic;

namespace ViasChileAuthAPI.Modelos
{
    public partial class CentroCostos
    {
        public CentroCostos()
        {
            EmpresaNiveles = new HashSet<EmpresaNiveles>();
        }

        public int CecoCod { get; set; }
        public string CecoNombre { get; set; }
        public int? VigeCod { get; set; }

        public virtual ICollection<EmpresaNiveles> EmpresaNiveles { get; set; }
    }
}
