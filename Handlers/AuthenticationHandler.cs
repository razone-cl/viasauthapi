﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using ViasChileAuthAPI.Modelos;

namespace ViasChileAuthAPI.Handlers
{
    public class AuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly SISACCESOContext _context;
        public AuthenticationHandler(
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            SISACCESOContext context)
            : base(options, logger, encoder, clock)
        {
            _context = context;
        }

        public object ClaimTypers { get; private set; }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey("Authorization"))
                 return AuthenticateResult.Fail("Authorization Header not Found");

            try
            {
                var authenticationHeaderValue = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
                var bytes = Convert.FromBase64String(authenticationHeaderValue.Parameter);
                string[] credentials = Encoding.UTF8.GetString(bytes).Split(":");
                string email = credentials[0];
                string password = credentials[1];

                Personas personaAutenticada = _context.Personas.Where(personaAutenticada => personaAutenticada.PersEmail == email &&
                                                                            personaAutenticada.PersPassword == password).FirstOrDefault();

                if (personaAutenticada == null)
                {
                    return AuthenticateResult.Fail("Email y/o Password incorrecto(s)");
                }
                else
                {
                    var claims = new[] { new Claim(ClaimTypes.Name, personaAutenticada.PersEmail) };
                    var identity = new ClaimsIdentity(claims, Scheme.Name);
                    var principal = new ClaimsPrincipal(identity);
                    var ticket = new AuthenticationTicket(principal, Scheme.Name);

                    return AuthenticateResult.Success(ticket);
                }
            }
            catch (Exception)
            {

                return AuthenticateResult.Fail("Se produjo un error.");
            }

            return AuthenticateResult.Fail("Falta Implementar");
        }
    }
 }
